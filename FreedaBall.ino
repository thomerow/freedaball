#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>

#define PIN 14
#define LED_COUNT 24

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, PIN, NEO_GRB + NEO_KHZ800);

WiFiUDP Udp;
const char* ssid = "Freeda Beast";
const char* password = "bummsn2018";
const unsigned int localPort = 8888;        // local port to listen for UDP packets (here's where we send the packets)
// char *address;

OSCErrorCode error;

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  // address = (char*) malloc(256 * sizeof(char));
} // setup

void setStripColor(uint32_t color) {
  for(int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, color);
  }
  strip.show();
}

uint32_t color = 0;

void setRed(int red) {
  Serial.print("R: ");
  Serial.println(red);
  color = (color & 0xFFFFu) | (red << 16);
  setStripColor(color);  
}

void red(OSCMessage &msg) {
  setRed(msg.getInt(0));
}

void setGreen(int green) {
  Serial.print("G: ");
  Serial.println(green);
  color = (color & 0xFF00FFu) | (green << 8);
  setStripColor(color);
}

void green(OSCMessage &msg) {
  setGreen(msg.getInt(0));
}

void setBlue(int blue) {
  Serial.print("B: ");
  Serial.println(blue);
  color = (color & 0xFFFFF00u) | blue;
  setStripColor(color);
}

void blue(OSCMessage &msg) {
  setBlue(msg.getInt(0));
}

void fader1(OSCMessage &msg) {
  setRed((int) (msg.getFloat(0) * 255));
}

void fader2(OSCMessage &msg) {
  setGreen((int) (msg.getFloat(0) * 255));
}

void fader3(OSCMessage &msg) {
  setBlue((int) (msg.getFloat(0) * 255));
}

void loop() {
  // OSCBundle bundle;
  OSCMessage message;
  int size = Udp.parsePacket();

  if (size > 0) {
    while (size--) {
      uint8_t b = Udp.read();
      message.fill(b);
      // Serial.print((char) b);
    }
    // Serial.println();
    if (!message.hasError()) {
      message.dispatch("/red", red);
      message.dispatch("/green", green);
      message.dispatch("/blue", blue);
      message.dispatch("/1/fader1", fader1);
      message.dispatch("/1/fader2", fader2);
      message.dispatch("/1/fader3", fader3);
    } 
    else {
      error = message.getError();
      Serial.print("error: ");
      Serial.println(error);
    }
  }
}
